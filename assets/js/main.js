$(document).ready( function(){
	var $hieuung = $('.portfolio_products').imagesLoaded( function() {
        $('.portfolio_products').isotope({
        itemSelector: '.portfolio_products--detail',
        layoutMode: 'masonry'
        })
    });


	$('.portfolio_title--items--name').click(function(event) {
		$('.portfolio_title--items--name').removeClass('active');
		$(this).addClass('active');
        $('.portfolio_title--items').removeClass('active');
        $(this).parent().addClass('active');
		ten = $(this).attr('href');
        setTimeout(function(){
            $hieuung.isotope({filter:ten})
        },100)
		return false;
	});


    $(window).scroll(function(){
        if ($(window).scrollTop() >=  700) {
            $('.menu').addClass('nav-fixed');
        }
        else {
            $('.menu').removeClass('nav-fixed');       
        }
    });

    // $(function() {
    //     var selectedClass = "";
    //     $(".portfolio_title--items--name").click(function(){ 
    //         $('.portfolio_title--items--name').removeClass('active');
    //         $(this).addClass('active');
    //         selectedClass = $(this).attr("data-rel");
    //         $(".portfolio_products").fadeTo(100, 0.1);
    //         $(".portfolio_products a").not("."+selectedClass).fadeOut().removeClass('scale-anm');
    //         setTimeout(function() {
    //             $("."+selectedClass).fadeIn().addClass('scale-anm');
    //             $(".portfolio_products").fadeTo(400, 1);
    //         }, 400); 
    //         return false
    //     });
    // });

	// var stt = 0;
	// firstSlide = parseInt($(".portfolio-single_detail_slider--contain:first").attr("data-stt"));
	// lastSlide = parseInt($(".portfolio-single_detail_slider--contain:last").attr("data-stt"));
 //    $(".portfolio-single_detail_slider--contain").each(function(){
 //     	if ($(this).is(':visible')){ 
 //        	stt = parseInt($(this).attr("data-stt"));
 //        }   
 //    })
 //    $('#next').click(function(event) {
 //    	next = ++stt;
 //    	if (next == lastSlide){
 //    		stt = (firstSlide-1);
 //    	}
 //    	$('.portfolio-single_detail_slider--contain').hide();
 //    	$('.portfolio-single_detail_slider--contain').eq(next).show(300);
 //    	console.log(next)
 //    	return false 
    	
 //    });
 //    $('#prv').click(function(event) {
 //    	if (stt == 0){
 //    		stt = (lastSlide+1);
 //    	}
 //    	prv = --stt;

 //    	$('.portfolio-single_detail_slider--contain').hide();
 //    	$('.portfolio-single_detail_slider--contain').eq(prv).show(300); 
 //    	console.log(prv)
 //    	return false 
 //    });

 	// lam cho portfolio single
	var stt = 0;
	firstSlide = parseInt($(".portfolio-single_detail_slider--contain:first").attr("data-stt"));
	lastSlide = parseInt($(".portfolio-single_detail_slider--contain:last").attr("data-stt"));
    $(".portfolio-single_detail_slider--contain").each(function(){
     	if ($(this).is(':visible')){ 
        	stt = parseInt($(this).attr("data-stt"));
        }   
    })
    $('#next').click(function(event) {
    	next = ++stt;
    	if (next == lastSlide){
    		stt = (firstSlide-1);
    	}
    	$('.portfolio-single_detail_slider--contain').removeClass('active');
    	$('.portfolio-single_detail_slider--contain').eq(next).addClass('active');
    	console.log(next)
    	return false 
    	
    });
    $('#prv').click(function(event) {
    	if (stt == 0){
    		stt = (lastSlide+1);
    	}
    	prv = --stt;
		$('.portfolio-single_detail_slider--contain').removeClass('active');
    	$('.portfolio-single_detail_slider--contain').eq(prv).addClass('active'); 
    	console.log(prv)
    	return false 
    });

	// lam cho about
	var astt = 0;
	afirstSlide = parseInt($(".about_bottom blockquote:first").attr("data-stt"));
	alastSlide = parseInt($(".about_bottom blockquote:last").attr("data-stt"));
    $(".portfolio-single_detail_slider--contain").each(function(){
     	if ($(this).is(':visible')){ 
        	astt = parseInt($(this).attr("data-stt"));
        }   
    })
    $('#next').click(function(event) {
    	next = ++astt;
    	if (next == alastSlide){
    		astt = (afirstSlide-1);
    	}
    	$('.about_bottom blockquote').removeClass('active');
    	$('.about_bottom blockquote').eq(next).addClass('active');
    	return false 
    	
    });
    $('#prv').click(function(event) {
    	if (astt == 0){
    		astt = (alastSlide+1);
    	}
    	prv = --astt;
		$('.about_bottom blockquote').removeClass('active');
    	$('.about_bottom blockquote').eq(prv).addClass('active'); 
    	return false 
    });
});